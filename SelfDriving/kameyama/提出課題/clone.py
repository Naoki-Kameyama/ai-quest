import sys
import csv
from tqdm import tqdm
import cv2
import datetime
import h5py
import numpy as np

from keras.models import Sequential, Model #変更
from keras.layers import Flatten, Dense, Lambda, Conv2D, Cropping2D, Dropout, MaxPooling2D
from keras.optimizers import Adam
from sklearn.model_selection import train_test_split

#ジェネレータの実装部分
IMAGE_HEIGHT, IMAGE_WIDTH, IMAGE_CHANNELS = 160, 320, 3
def batch_generator(inputs, outputs, batch_size):
    images = np.empty([batch_size, IMAGE_HEIGHT, IMAGE_WIDTH, IMAGE_CHANNELS])
    steers = np.empty(batch_size)
    start, end = 0, batch_size
    # ジェネレータが停止しないようにループさせる
    while True:
        i = 0
        if start >= inputs.shape[0]:
            start = 0
            end = batch_size
        for index in range(start, end):
            try:
                images[i] = inputs[index]
                steers[i] = outputs[index]
            except:
                pass
            i += 1
            if i == batch_size:
                start += batch_size
                end += batch_size
                break
        yield images, steers
#ジェネレータの呼び出し部分

def getrowsFromDrivingLogs(dataPath):
    rows = []
    with open(dataPath + '/driving_log.csv') as csvFile:
        reader = csv.reader(csvFile)            
        for row in reader:
            rows.append(row)
    return rows

def getImageArray3angle(imagePath, steering, images, steerings):
    originalImage = cv2.imread(imagePath.strip())
    image = cv2.cvtColor(originalImage, cv2.COLOR_BGR2RGB)
    
    img = cv2.flip(image,1)
    steering_flipped = -steering
    
    images.append(image)
    steerings.append(steering)
    
    images.append(img)
    steerings.append(steering_flipped)
    
def getImagesAndSteerings(rows):
    
    images = []
    steerings = []
    images_2 = []
    steerings_2 = []
    images_3 = []
    steerings_3 = []
    
    for row in tqdm(rows):
        #角度
        steering = float(row[3])
        parameter = 0.2 
        steering_left = steering + parameter
        steering_right = steering - parameter
        
        #center
        getImageArray3angle(row[0], steering, images, steerings)
        #left
        getImageArray3angle(row[1], steering_left, images_2, steerings_2)
        #right
        getImageArray3angle(row[2], steering_right, images_3, steerings_3)
        
    
    return (np.array(images), np.array(steerings))

def trainModelAndSave(model, inputs, outputs, epochs, batch_size):
    
    X_train, X_valid, y_train, y_valid = train_test_split(inputs, outputs, test_size=0.2, shuffle=True)
    #モデルの設定
    model.compile(loss='mean_squared_error', optimizer=Adam(lr=1.0e-4))
    #モデルの学習
    #model.fit(X_train, y_train, batch_size=batch_size, nb_epoch=epochs, verbose=1, validation_data=(X_valid, y_valid))
    ### 各エポックごとにトレーニングのロスと検証のロスをプロットします。　＃追加
    model.fit_generator(batch_generator( X_train, y_train, batch_size),
                    samples_per_epoch = X_train.shape[0],
                    nb_epoch = epochs,
                    max_q_size=1,
                    validation_data=batch_generator(X_valid, y_valid, batch_size),
                    nb_val_samples=len(X_valid),
                    verbose=1)
    model.save('model.h5')
    
#基本ネットワーク
def nn_model():
    model = Sequential()
    model.add(Flatten(input_shape=(160, 320, 3)))
    model.add(Dense(128, activation='relu'))
    model.add(Dense(1))
    return model

#畳み込みネットワーク
def cnn_model():
    model = Sequential()
    model.add(Conv2D(32, 3, 3, subsample=(2,2), activation='relu', input_shape=(160, 320, 3)))
    model.add(Conv2D(64, 3, 3, subsample=(2,2), activation='relu'))
    model.add(MaxPooling2D(pool_size=(2, 2)))
    model.add(Dropout(0.5))
    model.add(Flatten())
    model.add(Dense(128, activation='relu'))
    model.add(Dense(1))
    return model

#NVIDIA
def nvidia_model():
    model = Sequential()
    #cropping = ((crop_top, crop_bottom), (crop_left, crop_right)) (319,129), (1,50)
    #model.add(Cropping2D(cropping=((50,130), (1,319)), input_shape=(160,320,3))) #追加
    model.add(Cropping2D(cropping=((50,20), (1,1)), input_shape=(160,320,3))) 
    model.add(Lambda(lambda x: (x / 255.0) - 0.5, input_shape=(160, 320, 3)))
    model.add(Conv2D(24,5,5, subsample=(2,2), activation='elu'))
    model.add(Conv2D(36,5,5, subsample=(2,2), activation='elu'))
    model.add(Conv2D(48,5,5, subsample=(2,2), activation='elu'))
    model.add(Conv2D(64,3,3, activation='elu'))
    model.add(Conv2D(64,3,3, activation='elu'))
    model.add(Dropout(0.5))
    model.add(Flatten())
    model.add(Dense(100, activation='elu'))
    model.add(Dense(50, activation='elu'))
    model.add(Dense(10, activation='elu'))
    model.add(Dense(1))
    return model

if __name__ == "__main__":
    
    epochs = 10
    #バッチサイズは40以下にしてください。
    batch_size = 40
    is_dataset = True
    #is_dataset = False
    
    #is_dataset を「True」にすると前処理したデータセットを保存
    #is_dataset を「False」にすると、前処理済みの保存したデータセットを利用（前処理が不要になるため時短になります。一度前処理をしておく必要があります。）
    if is_dataset:
        print('get csv data from Drivinglog.csv')
        rows = getrowsFromDrivingLogs('data')
        print('preprocessing data...')
        inputs, outputs = getImagesAndSteerings(rows)
        
        with h5py.File('./trainingData.h5', 'w') as f:
            f.create_dataset('inputs', data=inputs)
            f.create_dataset('outputs', data=outputs)
    
    else:
        with h5py.File('./trainingData.h5', 'r') as f:
            inputs = np.array(f['inputs'])
            outputs = np.array(f['outputs'])

    print('Training data:', inputs.shape)
    print('Training label:', outputs.shape)
    
    #モデルの指定
    model = nvidia_model()
    #モデルの訓練と保存
    trainModelAndSave(model, inputs, outputs, epochs, batch_size)