import numpy as np
# pandasのインポート
import pandas as pd
import datetime as dt
# matplotlib.pyplotのインポート
import matplotlib
import matplotlib.pyplot as plt
# seabornのインポート
import seaborn as sns
# train_test_splitのインポート
import sklearn
from sklearn.model_selection import train_test_split
# 決定木モデルのインポート
from sklearn.tree import DecisionTreeClassifier as DT
from pandas import Series,DataFrame
from sklearn.datasets import load_boston
from sklearn.linear_model import LinearRegression

# データの読み込み
df = pd.read_csv('./data/train.csv')
dateframe_original = pd.read_csv('./data/test.csv')
df_ft = pd.read_csv('./data/test.csv')
df_sample = pd.read_csv('./data/sample_submit.csv')

# いらないデータをdrop
df = df.drop(['name', 'remarks','weather','precipitation'], axis=1)
df_ft = df_ft.drop(['name', 'remarks','weather','precipitation'], axis=1)

#indexをdatetimeにセット
df = df.set_index('datetime')
df_ft = df_ft.set_index('datetime')

#dummy変数化
df = pd.get_dummies(df)
df_ft = pd.get_dummies(df_ft)

# 説明変数をdata_Xに、目的変数をdata_yに代入
data_X = df.drop(['y','kcal', 'payday'], axis=1) #kcalも
data_ft = df_ft.drop(['kcal', 'payday'], axis=1)
print(data_ft)

#data_X = data_X.dropna(); 
data_y = df['y']

# 学習データと評価データにデータを分割
X_train, X_test, Y_train, Y_test = sklearn.model_selection.train_test_split(data_X,data_y)

#モデルを作成
linear_regression = LinearRegression()
linear_regression.fit(X_train, Y_train)
linear_regression.score(X_train, Y_train)
print( data_X.describe() )

# 回帰係数
print(linear_regression.coef_)
 
# 切片 (誤差)
print(linear_regression.intercept_)
 
# 決定係数
print(linear_regression.score(X_train, Y_train))


# 予測結果
result = linear_regression.predict(X_test)
result_ft = linear_regression.predict(data_ft)

# 需要予測数値(トレーニング)
df_result = pd.DataFrame({
    "Y_test":Y_test,
    "result":result
})

# 需要予測数値(本番)
df_ft_result = pd.DataFrame({
    "result":result_ft
})

# 需要予測数値(本番)のデータフレーム化
df_ft_result = pd.DataFrame(data=df_ft_result)

# final_resultのデータフレームのindexをdatetimeに変更
df_concat = pd.concat([df_ft_result, dateframe_original],sort=False,axis=1)
df_concat = df_concat.set_index('datetime')
final_result = df_concat['result']

final_result.to_csv('submit.csv')

# 結果出力
final_result.plot(figsize=(15, 3))
plt.show()