# pandasのインポート
import pandas as pd

# データの読み込み
df = pd.read_csv('cor.csv')

# 量的データの相関係数の計算
corr_matrix = df.corr()

# matplotlib.pyplotのインポート
import matplotlib.pyplot as plt

# seabornのインポート
import seaborn as sns

# heatmapを作成
sns.heatmap(corr_matrix, cmap="Reds")

# グラフにタイトルを追加
plt.title('Correlation')

# グラフを表示
plt.show()
