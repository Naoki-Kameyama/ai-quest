public class BatchDateUpdateBL extends CollectionBusinessLogicBase {
   @Override
    public void process(ChMessage mes, ChParameter param) throws ChronosException {
        // バーチャル口座情報のBeanを取得
        CMCollectionBean cmBean = (CMCollectionBean)mes.getNormalMsg().getValue().getValue();
        Collection20012Bean collection20012Bean = cmBean.getCollection20012Bean();
        
        try {
            // １．変数初期化
            String vertualKozaInfo = collection20012Bean.getGinkoCode() + collection20012Bean.getSitenCode() 
            int MaxQuantity = 0

            //  ２．バーチャル口座情報に対して各種チェックを行う。
            if (◆バーチャル口座登録DTO.処理区分　＝　空（NULL　OR　空白文字列）) {
                //  ａ）エラー情報を設定する
                バーチャル口座登録DTO.エラー理由ID　　＝　バーチャル口座登録エラーコード.FORMAT_DUMMY.値（"01"）
                バーチャル口座登録DTO.エラー理由名称　＝　バーチャル口座登録エラーコード.FORMAT_DUMMY.論理名（"形式エラー（項目数）"）
                ｂ）【内部関数１－１】.バーチャル口座エラー情報蓄積処理を実施する。
            }

            if (◆バーチャル口座登録DTO.処理区分の形式　≠　１桁英数字) {
                // ａ）エラー情報を設定する
                バーチャル口座登録DTO.エラー理由ID　　＝　バーチャル口座登録エラーコード.FORMAT_DUMMY.値（"02"）
                バーチャル口座登録DTO.エラー理由名称　＝　バーチャル口座登録エラーコード.FORMAT_DUMMY.論理名（"形式エラー（データ桁）"）
                ｂ）【内部関数１－１】.バーチャル口座エラー情報蓄積処理を実施する。
            }

            /*
            ・
            ・
            ・
            */

            // ２．２．銀行マスタデータ存在チェック
            BankInfo bankInfo = this.BankInfoLinkWrapper.bankInfoGet(collection20012Bean.getGinkoCode());
            if (◆（bankInfo　＝　null）　OR　（bankInfo.size()　＝　0）の場合) {
                (１）．エラー情報を設定する	
	                バーチャル口座登録DTO.エラー理由ID　　＝　バーチャル口座登録エラーコード.BANK_NOT_EXISTS.値（"03"）
	                バーチャル口座登録DTO.エラー理由名称　＝　バーチャル口座登録エラーコード.BANK_NOT_EXISTS.論理名（"銀行マスタ未存在"）
                （２）．【内部関数１－１】.バーチャル口座エラー情報蓄積処理を実施する。	
            }

            // ２．３．口座管理情報チェック
            同上


            // ３．パラメータリファレンスから、バーチャル口座のグループの１グループの最大口座数を取得
            // 共通関数の「パラメータリファレンス取得（102100080）」を呼び出し、１グループ最大バーチャル口座件数のリファレンスを取得する。
            CollectionCommonLogic collectionCommonLogic = new CollectionCommonLogic()
            MaxBankGroup maxBankGroup = collectionCommonLogic.getParmeterReference(102100080)
            if (◆（maxBankGroup　＝　null）　OR　（maxBankGroup.size()　＝　0）の場合) {
                （１）．以下のメッセージをログ出力する.
		            メッセージID：PMT-E800006、パラメータ：102100080
                （２）．バーチャル口座登録フローを終了する。
                    exit()
            }

            変数.１グループ最大バーチャル口座件数　＝　パラメータリファレンス情報リスト（０）.整数型パラメータ値
            int maxBankGroupNum = maxBankGroup[0].getNum()

            //  ４．バーチャル口座情報登録
            // ４．１．バーチャル口座情報登録・利用停止を判定
            if (◆バーチャル口座登録DTO.処理区分　＝　新規) {
                ４．２．バーチャル口座情報登録を実施する。
                バーチャル口座グループ管理キャッシュから、バーチャル口座グループ管理情報を取得する。
                BankContInfo bankContInfo = this.BankContInfoLinkWrapper.bankContInfoGet(collection20012Bean.getvalue(“VIRTUAL_ACCOUNT_GROUP_ADMIN_KEY”));
                if (◆（bankContInfo　＝　null）　OR　（bankContInfo.size()　＝　0）の場合) {
                    （１）．バーチャル口座グループ情報登録				
	                    // ①バーチャル口座グループ情報のインスタンス化する。			
		                変数.バーチャル口座グループ情報　←　new　バーチャル口座グループ情報（）		
		                変数.バーチャル口座グループ情報リスト　←　new　バーチャル口座グループ情報リスト（）		
		                変数.バーチャル口座グループ情報詳細　←　new　バーチャル口座グループ情報詳細（）	
                    ②変数.バーチャル口座グループ情報詳細を設定する。
                    ③バーチャル口座グループ情報リストに追加する。		
	                    変数.バーチャル口座グループ情報リスト　add　変数.バーチャル口座グループ情報詳細
                    ④バーチャル口座グループ情報を設定する		
                    	変数.バーチャル口座グループ情報.バーチャル口座グループ情報リスト　←　上記③の変数.バーチャル口座グループ情報リスト	
	                    変数.バーチャル口座グループ情報.バーチャル口座グループコード　←　共通関数の「ユニークID生成」で生成したユニークID	
                    ⑤バーチャル口座グループ情報データアクセスラッピング「バーチャル口座グループ情報の登録（変数．バーチャル口座グループ情報）」を呼び出して、バーチャル口座グループ情報を登録する。
                    （２）．バーチャル口座情報登録			
	                    ①バーチャル口座情報のインスタンス化する。		
		                    変数.バーチャル口座情報　←　new　バーチャル口座情報（）	
                    	②変数.バーチャル口座情報を設定する。
                        ③バーチャル口座情報データアクセスラッピング「バーチャル口座情報の登録（変数．バーチャル口座情報）」を呼び出して、バーチャル口座情報を登録する。
                    （３）．バーチャル口座グループ管理情報登録		
                    	①バーチャル口座グループ管理情報のインスタンス化する。	
		                    変数.バーチャル口座グループ管理情報　←　new　バーチャル口座グループ管理情報（）
		                    変数.バーチャル口座グループ管理情報リスト　←　new　バーチャル口座グループ管理情報リスト（）
		                    変数.バーチャル口座グループ管理情報詳細　←　new　バーチャル口座グループ管理情報詳細（）
                        ②変数.バーチャル口座グループ管理情報詳細を設定する
                        ③バーチャル口座グループ管理情報リストに追加する。	
	                        変数.バーチャル口座グループ管理情報リスト　add　変数.バーチャル口座グループ管理情報詳細
                        ④バーチャル口座グループ管理情報を設定する		
	                        変数.バーチャル口座グループ管理情報.バーチャル口座グループ管理情報リスト　←　上記③の変数.バーチャル口座グループ管理情報リスト	
	                        変数.バーチャル口座グループ管理情報.バーチャル口座グループ管理キー　←　“VIRTUAL_ACCOUNT_GROUP_ADMIN_KEY”	
                        ⑤バーチャル口座グループ管理情報データアクセスラッピング「バーチャル口座グループ管理情報の登録（変数．バーチャル口座グループ管理情報）」を呼び出して、バーチャル口座グループ管理情報を登録する。
                } else {
                    
                }


            } else {
                ４．３．バーチャル口座情報利用停止を実施する。
            }


        } catch (ChronosBusinessLogicException e) {
            throw e.getException();
        }
    }
}