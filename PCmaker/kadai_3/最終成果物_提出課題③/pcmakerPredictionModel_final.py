import numpy as np
# pandasのインポート
import pandas as pd
import datetime as dt
# matplotlib.pyplotのインポート
import matplotlib
import matplotlib.pyplot as plt
# seabornのインポート
import seaborn as sns
# train_test_splitのインポート
import sklearn
from sklearn.model_selection import train_test_split
# 決定木モデルのインポート
from sklearn.tree import DecisionTreeClassifier as DT
from pandas import Series,DataFrame
from sklearn.linear_model import LogisticRegression
from sklearn.preprocessing import OrdinalEncoder
from sklearn.preprocessing import LabelEncoder

pd.set_option('display.max_columns', 100)
pd.set_option('display.max_rows', 100)

# 訓練データの生成
purchase = pd.read_csv('./data/purchase_record.csv')
user = pd.read_csv('./data/user_info.csv')
df = pd.merge(purchase, user, how="left" ,on="user_id")

# アウトプットデータの生成
test = pd.read_csv('./data/purchase_record_test.csv')
df_ft = pd.merge(test, user, how="left" ,on="user_id")
out = df_ft

#indexをdatetimeにセット
df = df.set_index('purchase_id')
df_ft = df_ft.set_index('purchase_id')

#bool値を0,1に変換
df = df * 1
df_ft = df_ft * 1

#LabelEncodingのためのデータを整形（これをやらないと次の過程で時間がかかる）
df = df.fillna("NaN")
df_ft = df_ft.fillna("NaN")

#labelEncoding

for column in ['user_id','date_x','date_y','product_id','parts_1','parts_2','parts_3','parts_4','parts_5','parts_6','parts_7','parts_8','parts_9',
'attribute_1','attribute_2','attribute_3','attribute_4','attribute_5','attribute_6','attribute_7','attribute_8','attribute_9','attribute_10','attribute_11','attribute_12','attribute_13','attribute_14','attribute_15','attribute_16','attribute_17','attribute_18','attribute_19','attribute_20','attribute_21','attribute_22','attribute_23','attribute_24','attribute_25','attribute_26','attribute_27','attribute_28','attribute_29','attribute_30'
]:
    le = LabelEncoder()
    le.fit(df[column])
    df[column] = le.transform(df[column])
    print(column)

for column_df in ['user_id','date_x','date_y','product_id','parts_1','parts_2','parts_3','parts_4','parts_5','parts_6','parts_7','parts_8','parts_9',
'attribute_1','attribute_2','attribute_3','attribute_4','attribute_5','attribute_6','attribute_7','attribute_8','attribute_9','attribute_10','attribute_11','attribute_12','attribute_13','attribute_14','attribute_15','attribute_16','attribute_17','attribute_18','attribute_19','attribute_20','attribute_21','attribute_22','attribute_23','attribute_24','attribute_25','attribute_26','attribute_27','attribute_28','attribute_29','attribute_30'
]:
    le_ft = LabelEncoder()
    le_ft.fit(df_ft[column_df])
    df_ft[column_df] = le_ft.transform(df_ft[column_df])
    print(column_df)

#drop
#df = df.drop(['date_x','date_y','parts_1','parts_2','parts_3','parts_4','parts_5','parts_6','parts_7','parts_8','parts_9','user_id'], axis=1)
#df_ft = df_ft.drop(['date_x','date_y','parts_1','parts_2','parts_3','parts_4','parts_5','parts_6','parts_7','parts_8','parts_9','user_id'], axis=1)


# 説明変数をdata_Xに、目的変数をdata_yに代入
data_X = df.drop(['purchase'], axis=1)
data_y = df['purchase']
data_ft = df_ft

# 学習データと評価データにデータを分割
X_train, X_test, Y_train, Y_test = sklearn.model_selection.train_test_split(data_X,data_y)

#モデルを作成
lr = LogisticRegression()
lr.fit(X_train, Y_train)
lr.score(X_train, Y_train)

# 予測結果
result = lr.predict(X_test)
result_ft = lr.predict(data_ft)

# 確率の導出
proba = lr.predict_proba(data_ft)[:, 1]

# 出力データの整形
df_ft_result = pd.DataFrame({
    "probability":proba
})
df_ft_result = pd.DataFrame(data=df_ft_result)
df_concat = pd.concat([df_ft_result, out],sort=False,axis=1)
df_concat = df_concat.sort_values(by='probability', ascending=False)

submit_df = df_concat[['purchase_id', 'probability']]
df_concat = df_concat.set_index('purchase_id')

output = df_concat['probability']

final_result = output
print(final_result)

# csvに書き出し
final_result.to_csv('./submit3.csv')
