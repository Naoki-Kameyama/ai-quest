import numpy as np
# pandasのインポート
import pandas as pd
import datetime as dt
# matplotlib.pyplotのインポート
import matplotlib
import matplotlib.pyplot as plt
# seabornのインポート
import seaborn as sns
# train_test_splitのインポート
import sklearn
from sklearn.model_selection import train_test_split
# 決定木モデルのインポート
from sklearn.tree import DecisionTreeClassifier as DT
from pandas import Series,DataFrame
from sklearn.linear_model import LogisticRegression

# データの読み込み
print("データの読み込み")
purchase = pd.read_csv('./data/purchase_record.csv')
user = pd.read_csv('./data/user_info.csv')
test = pd.read_csv('./data/purchase_record_test.csv')

# purchaseとuserの表を統合
print("統合")
Integration_cust = pd.merge(purchase, user, how="inner" ,on="user_id")
Integration_test = pd.merge(test, user, how="inner" ,on="user_id") #test
#Integration_cust = Integration_cust.fillna(0, inplace=True)
print(type(Integration_cust))


#統合データの整形
print("整形")
Integration_cust.dropna(0)
Integration_test.dropna(0) #test
pd.set_option('display.max_columns', 100)
Integration_cust = Integration_cust.replace(np.nan,0, regex=True)
Integration_test = Integration_test.replace(np.nan,0, regex=True) #test

print(Integration_cust.head())
print(Integration_cust.index)

#データを分割
print("分割")
Itg_1 = Integration_cust[:5000]
""" Itg_2 = Integration_cust[100001:200000]
Itg_3 = Integration_cust[200001:300000]
Itg_4 = Integration_cust[300001:400000]
Itg_5 = Integration_cust[400001:500000]
Itg_6 = Integration_cust[500001:600000]
Itg_7 = Integration_cust[600001:700000]
Itg_8 = Integration_cust[700001:800000]
Itg_9 = Integration_cust[800001:900000]
Itg_10 = Integration_cust[900001:1000000]
Itg_11 = Integration_cust[1000001:1100000]
Itg_12 = Integration_cust[1100001:1200000]
Itg_13 = Integration_cust[1200001:1300000]
Itg_14 = Integration_cust[1300001:1352679] """

Test_1 = Integration_test[:5000] #test


# Integration_cust.to_csv('submit.csv')

#dummy変数化
print("dummy化")
Itg_dummy = pd.get_dummies(Itg_1)
test = pd.get_dummies(Test_1) #test

print(Itg_dummy.head())


# 説明変数をdata_Xに、目的変数をdata_yに代入
data_X = Itg_dummy.drop(columns='purchase')
data_y = Itg_dummy['purchase']
print(data_y)

print("shape")
print(test.shape) #(5000, 6666)
print(data_X.shape) #(5000, 6173)

data_X.to_csv('./data/data_X.csv')
data_y.to_csv('./data/data_y.csv')
test.to_csv('./data/test.csv')

# 学習データと評価データにデータを分割
X_train, X_test, Y_train, Y_test = sklearn.model_selection.train_test_split(data_X,data_y)

#モデルを作成
print("モデルを作成")
lr = LogisticRegression(solver='liblinear') # ロジスティック回帰モデルのインスタンスを作成
lr.fit(X_train, Y_train) # ロジスティック回帰モデルの重みを学習
print("fit")
print(lr.score(X_train, Y_train))
print("score")
print(lr.classes_)
print("test_score")
print(lr.score(X_test, Y_test))

proba = lr.predict_proba(test)[:, 1]

print(proba)
#Test_1['probability'] = proba
#submit_df = Test_1[['purchase_id', 'probability']]
#print(submit_df.head())



# 予測結果
# result = linear_regression.predict(X_test)

""" # いらないデータをdrop
df = df.drop(['name', 'remarks','weather','precipitation'], axis=1)
df_ft = df_ft.drop(['name', 'remarks','weather','precipitation'], axis=1)

#indexをdatetimeにセット
df = df.set_index('datetime')
df_ft = df_ft.set_index('datetime')

#dummy変数化
df = pd.get_dummies(df)
df_ft = pd.get_dummies(df_ft)

# 説明変数をdata_Xに、目的変数をdata_yに代入
data_X = df.drop(['y','kcal', 'payday'], axis=1) #kcalも
data_ft = df_ft.drop(['kcal', 'payday'], axis=1)
print(data_ft)

#data_X = data_X.dropna(); 
data_y = df['y']

# 学習データと評価データにデータを分割
X_train, X_test, Y_train, Y_test = sklearn.model_selection.train_test_split(data_X,data_y)

#モデルを作成
linear_regression = LinearRegression()
linear_regression.fit(X_train, Y_train)
linear_regression.score(X_train, Y_train)
print( data_X.describe() )

# 回帰係数
print(linear_regression.coef_)
 
# 切片 (誤差)
print(linear_regression.intercept_)
 
# 決定係数
print(linear_regression.score(X_train, Y_train))


# 予測結果
result = linear_regression.predict(X_test)
result_ft = linear_regression.predict(data_ft)

# 需要予測数値(トレーニング)
df_result = pd.DataFrame({
    "Y_test":Y_test,
    "result":result
})

# 需要予測数値(本番)
df_ft_result = pd.DataFrame({
    "result":result_ft
})

# 需要予測数値(本番)のデータフレーム化
df_ft_result = pd.DataFrame(data=df_ft_result)

# final_resultのデータフレームのindexをdatetimeに変更
df_concat = pd.concat([df_ft_result, dateframe_original],sort=False,axis=1)
df_concat = df_concat.set_index('datetime')
final_result = df_concat['result']

final_result.to_csv('submit.csv')

# 結果出力
final_result.plot(figsize=(15, 3))
plt.show() """