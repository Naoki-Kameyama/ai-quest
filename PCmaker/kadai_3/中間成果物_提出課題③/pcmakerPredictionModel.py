import numpy as np
# pandasのインポート
import pandas as pd
import datetime as dt
# matplotlib.pyplotのインポート
import matplotlib
import matplotlib.pyplot as plt
# seabornのインポート
import seaborn as sns
# train_test_splitのインポート
import sklearn
from sklearn.model_selection import train_test_split
# 決定木モデルのインポート
from sklearn.tree import DecisionTreeClassifier as DT
from pandas import Series,DataFrame
from sklearn.linear_model import LogisticRegression

# 訓練データの生成
purchase = pd.read_csv('./data/purchase_record.csv')
user = pd.read_csv('./data/user_info.csv')
df = pd.merge(purchase, user, how="left" ,on="user_id")

# アウトプットデータの生成
test = pd.read_csv('./data/purchase_record_test.csv')
df_ft = pd.merge(test, user, how="left" ,on="user_id")
out = df_ft[:21006]

#indexをdatetimeにセット
df = df.set_index('purchase_id')
df_ft = df_ft.set_index('purchase_id')

#データを整形
df = df.dropna(0)
df_ft = df_ft.dropna(0) 

df = df.drop(['date_x','date_y','parts_1','parts_2','parts_3','parts_4','parts_5','parts_6','parts_7','parts_8','parts_9','user_id'], axis=1)
df_ft = df_ft.drop(['date_x','date_y','parts_1','parts_2','parts_3','parts_4','parts_5','parts_6','parts_7','parts_8','parts_9','user_id'], axis=1)


pd.set_option('display.max_columns', 100)
pd.set_option('display.max_rows', 100)
print("--------------df,統合----------------")
print(df.head())
print("--------------df_ft,統合----------------")
print(df_ft.head())

#dummy変数化
df = pd.get_dummies(df)
df_ft = pd.get_dummies(df_ft)
print("--------------df,dummy----------------")
print(df.head())
print(df.shape)
print("--------------df_ft,dummy----------------")
print(df_ft.head())
print(df_ft.shape)

# 説明変数をdata_Xに、目的変数をdata_yに代入
data_X = df.drop(['purchase'], axis=1)
data_y = df['purchase']

data_ft = df_ft


# 学習データと評価データにデータを分割
X_train, X_test, Y_train, Y_test = sklearn.model_selection.train_test_split(data_X,data_y)

#モデルを作成
lr = LogisticRegression()
lr.fit(X_train, Y_train)
lr.score(X_train, Y_train)
print( data_X.describe() )

# 予測結果
result = lr.predict(X_test)
result_ft = lr.predict(data_ft)

#print(result)
#print(result_ft)

proba = lr.predict_proba(data_ft)[:, 1]
print(proba.shape)
print(out.shape)

df_ft_result = pd.DataFrame({
    "probability":proba
})
df_ft_result = pd.DataFrame(data=df_ft_result)
df_concat = pd.concat([df_ft_result, out],sort=False,axis=1)
df_concat = df_concat.sort_values(by='probability', ascending=False)

df_concat = df_concat.set_index('purchase_id')
print(df_concat.head())

output = df_concat['probability']

final_result = output[:15]
print(final_result.head())

final_result.to_csv('./my_submit.csv')
