# タクシー乗降データ

1) データ項目
#train.csv:
- id: レコードを判別するためのID
- pick_datetime: 乗車時の日時時間（日本標準時系）
- pick_dayofweek: 乗車時の曜日。1(日曜日)~7(土曜日)
- pick_latitude: 乗車位置の緯度
- pick_longitude: 乗車位置の経度
- drop_datetime: 降車時の日時時間（日本標準時系）
- drop_dayofweek: 降車時の曜日。1(日曜日)~7(土曜日)
- drop_latitude: 降車位置の緯度
- drop_longitude: 降車位置の経度
※データは提供企業からの要望により機密情報保護のため、西暦を2260年に変更、乗車時間・降車時間について標準偏差7分のノイズがかけられています。

#submit.csv:（※submit.csvにはヘッダは含めず、1行目から回答値としてください）
- id: レコードを判別するためのID
- trip_duration: 予測した乗車時間（秒）




