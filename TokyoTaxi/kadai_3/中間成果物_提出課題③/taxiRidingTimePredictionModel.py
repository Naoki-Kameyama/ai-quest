import numpy as np
# pandasのインポート
import pandas as pd
import datetime as dt
#import datetime
# matplotlib.pyplotのインポート
import matplotlib
import matplotlib.pyplot as plt
# seabornのインポート
import seaborn as sns
# train_test_splitのインポート
import sklearn
from sklearn.model_selection import train_test_split
# 決定木モデルのインポート
from sklearn.tree import DecisionTreeClassifier as DT
from pandas import Series,DataFrame
from sklearn.linear_model import LogisticRegression
import time
from sklearn.linear_model import LinearRegression
import math

# データの読み込み
print(str(dt.datetime.now())+": "+"データの読み込み")
df_train = pd.read_csv('./data/train.csv')
df_test = pd.read_csv('./data/test.csv')
print(df_train.shape)
df_plot = df_train.drop(['id','pick_datetime','pick_dayofweek','drop_datetime','drop_dayofweek','drop_latitude','drop_longitude'], axis=1)


# 外れ値除外処理(pick)
print(str(dt.datetime.now())+": "+"外れ値除外処理(pick)")
df_train = df_train[df_train['pick_latitude'] > 5]
df_train = df_train[df_train['pick_longitude'] > 5]
x = df_train[df_train['pick_latitude'] > 5]['pick_latitude']
y = df_train[df_train['pick_longitude'] > 5]['pick_longitude']

# 外れ値除外処理(drop)
print(str(dt.datetime.now())+": "+"外れ値除外処理(drop)")
df_train = df_train[df_train['drop_latitude'] > 5]
df_train = df_train[df_train['drop_longitude'] > 5]
x = df_train['drop_latitude']
y = df_train['drop_longitude']

# 緯度経度からユークリッド距離の算出(train)
print(str(dt.datetime.now())+": "+"緯度経度からユークリッド距離の算出(train)")
a1 = df_train['pick_latitude'] - df_train['drop_latitude']
b1 = df_train['pick_longitude'] - df_train['drop_longitude']
c1 = a1**2+b1**2
print(np.sqrt(c1))
df_train['distance'] = np.sqrt(c1)

# 緯度経度からユークリッド距離の算出(test)
print(str(dt.datetime.now())+": "+"緯度経度からユークリッド距離の算出(test)")
a1 = df_test['pick_latitude'] - df_test['drop_latitude']
b1 = df_test['pick_longitude'] - df_test['drop_longitude']
c1 = a1**2+b1**2
print(np.sqrt(c1))
df_test['distance'] = np.sqrt(c1)
 
# 散布図を描画
# plt.scatter(x, y,marker='.', color='green')
# plt.show()

# 乗車時間[s]の算出
print(str(dt.datetime.now())+": "+"乗車時間[s]の算出")
drop_datetime = pd.to_datetime(df_train['drop_datetime'])
pick_datetime = pd.to_datetime(df_train['pick_datetime'])
df_train["riding_time"] = (drop_datetime - pick_datetime).dt.total_seconds()

# 乗車時間の分解(train)
print(str(dt.datetime.now())+": "+"乗車時間の分解(train)")
df_train['day'] = pick_datetime.dt.day
df_train['hour'] = pick_datetime.dt.hour
df_train['minute'] = pick_datetime.dt.minute

# 乗車時間の分解(test)
print(str(dt.datetime.now())+": "+"乗車時間の分解(test)")
pick_datetime_test = pd.to_datetime(df_test['pick_datetime'])
df_test['day'] = pick_datetime_test.dt.day
df_test['hour'] = pick_datetime_test.dt.hour
df_test['minute'] = pick_datetime_test.dt.minute

# トレーニングデータから不要要素の削除
print(str(dt.datetime.now())+": "+"トレーニングデータから不要要素の削除")
df_train = df_train.drop(['drop_datetime', 'pick_datetime','pick_latitude', 'drop_latitude', 'pick_longitude', 'drop_longitude'], axis=1)

# 説明変数をdata_Xに、目的変数をdata_yに代入
print(str(dt.datetime.now())+": "+"説明変数をdata_Xに、目的変数をdata_yに代入")
data_X = df_train.drop(['riding_time','id','drop_dayofweek'], axis=1)
data_y = df_train["riding_time"] 

# テストデータもトレーニングデータに合わせて、idをdrop
df_test = df_test.drop(['id', 'pick_datetime','pick_latitude', 'drop_latitude', 'pick_longitude', 'drop_longitude'], axis=1)

# 学習データと評価データにデータを分割
print(str(dt.datetime.now())+": "+"学習データと評価データにデータを分割")
X_train, X_test, Y_train, Y_test = sklearn.model_selection.train_test_split(data_X,data_y)

# モデルを作成
print(str(dt.datetime.now())+": "+"モデルを作成")
print(str(dt.datetime.now())+": "+"LinearRegressionインスタンスの作成")
linear_regression = LinearRegression()
print(str(dt.datetime.now())+": "+"LinearRegressionインスタンスの学習")
linear_regression.fit(X_train, Y_train)
# モデルの詳細
print(str(dt.datetime.now())+": "+"モデルの詳細")
print( data_X.describe() )
# 回帰係数
print(str(dt.datetime.now())+": "+"回帰係数")
print(linear_regression.coef_)
# 切片
print(str(dt.datetime.now())+": "+"切片")
print(linear_regression.intercept_)
# 決定係数
print(str(dt.datetime.now())+": "+"決定係数")
print(linear_regression.score(X_train, Y_train))

# 予測結果
print("トレーニングデータの予測結果")
print(X_test)
result_train = linear_regression.predict(X_test)
print(result_train)
print("テストデータの予測結果")
print(df_test)
result_test = linear_regression.predict(df_test)
print(type(result_test))


# 提出用データフレームを作成
# データフレームを作成
df_submit = pd.read_csv('./data/test.csv')
df_submit['result'] = result_test
df_test['result'] = result_test

# CSV ファイル (submit.csv) として出力
df_submit.to_csv("./data/submit.csv", columns=['id', 'result'], index=False, header=False)

# データの読み込み
#df = pd.read_csv('./data/cor.csv')
# 量的データの相関係数の計算
#corr_matrix = df.corr()
# heatmapを作成
#sns.heatmap(corr_matrix, cmap="Reds")
# グラフにタイトルを追加
#plt.title('Correlation')
# グラフを表示
#plt.show()

#x = df_test['result']
#y = df_submit['pick_datetime']
# 散布図を描画
#plt.scatter(x, y,marker='.', color='green')
#plt.show()
